package se.clearchannel.citybikes.api;

/**
 * Created by perj on 2014-03-14.
 */


    import java.io.BufferedInputStream;
    import java.io.File;
    import java.io.FileOutputStream;
    import java.io.IOException;
    import java.io.InputStream;
    import java.net.URL;
    import java.net.URLConnection;
    import java.util.ArrayList;
    import java.util.Calendar;
    import java.util.Date;
    import java.util.HashMap;
    import java.util.Map;

    import org.apache.http.util.ByteArrayBuffer;

    import android.content.ActivityNotFoundException;
    import android.content.Context;
    import android.content.Intent;
    import android.net.ConnectivityManager;
    import android.net.Uri;
    import android.os.Environment;
    import android.support.v4.app.FragmentActivity;
    import android.util.Log;

    public abstract class FileHandler {

        public static final String FILE_NAME = "CB_karta_cykelstationer_%s.pdf";  //put the downloaded file here

        public static Map<String, String> URL_PATHS;
        static
        {
            URL_PATHS = new HashMap<String, String>();
            URL_PATHS.put("Stockholm", "http://www.citybikes.se/Uploads/CB_STHLM_karta_cykelstationer_");
            URL_PATHS.put("Uppsala", "http://www.citybikes.se/Uploads/CB_UPPSALA_karta_cykelstationer_");
        }
        public static void DownloadFromUrl(Context ctx) {  //this is the downloader method
            for(Map.Entry<String, String> cursor : URL_PATHS.entrySet())
            {
                try {

                    String path = ctx.getExternalFilesDir(null).getPath() + String.format("/CB_karta_cykelstationer_%s.pdf", cursor.getKey());

                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);


                    URL url = new URL(cursor.getValue() + year + ".pdf" ); //you can write here any link
                    File file = new File(path);
                    if(!file.exists())
                    {long startTime = System.currentTimeMillis();
                        /* Open a connection to that URL. */
                    URLConnection ucon = url.openConnection();

                        /*
                         * Define InputStreams to read from the URLConnection.
                         */
                    InputStream is = ucon.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);

                        /*
                         * Read bytes to the Buffer until there is nothing more to read(-1).
                         */
                    ByteArrayBuffer baf = new ByteArrayBuffer(50);
                    int current = 0;
                    while ((current = bis.read()) != -1) {
                        baf.append((byte) current);
                    }

                        /* Convert the Bytes read to a String. */
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(baf.toByteArray());
                    fos.close();
                    Log.d("ImageManager", "download ready in"
                            + ((System.currentTimeMillis() - startTime) / 1000)
                            + " sec");
                    }
                } catch (IOException e) {
                    Log.d("ImageManager", "Error: " + e);
                }
            }
        }





        public static boolean offLineMapExist(Context ctx, String city)
        {
            String path = ctx.getExternalFilesDir(null).getPath() + "/" + String.format(FILE_NAME, city);
                File file = new File(path);
                return file.exists();

        }

        public static File getFile(Context ctx, String city)
        {
            String path = ctx.getExternalFilesDir(null).getPath()  + "/" + String.format(FILE_NAME, city);

            return new File(path);
        }

        public static CharSequence[] getCities()
        {

            ArrayList<String> list = new ArrayList<String>();
            for(Map.Entry<String, String> cursor : URL_PATHS.entrySet())
            {
                list.add(cursor.getKey());
            }
            return list.toArray(new CharSequence[list.size()]);
        }


    }

