package se.clearchannel.citybikes.api.domain;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name="racks")
public class RackList {

	@ElementList(inline=true)
	private List<BikeRack> racks;
	
	public List<BikeRack> getRacks()
	{
		return racks;
	}
	
	public void setRacks(List<BikeRack> racks)
	{
		this.racks = racks;
	}
}
