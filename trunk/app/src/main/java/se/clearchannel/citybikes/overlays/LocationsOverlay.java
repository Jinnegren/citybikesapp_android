package se.clearchannel.citybikes.overlays;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.Toast;
import se.clearchannel.citybikes.R;
import se.clearchannel.citybikes.ballonview.BalloonItemizedOverlay;
import se.clearchannel.citybikes.ballonview.BalloonOverlayView;
import se.clearchannel.citybikes.locations.BikeLocation;
import se.clearchannel.citybikes.locations.BikeLocationData;
import se.clearchannel.citybikes.locations.BikeLocationData.RackStatus;
import se.clearchannel.citybikes.locations.LocationProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LocationsOverlay {

	private ArrayList<BikeLocation> bikeLocations = new ArrayList<BikeLocation>();
	private Context context;
	ProgressDialog dialog;
	Map<RackStatus, Drawable> markers;
	/*
	public LocationsOverlay(Drawable defaultMarker, MapView mapView, LocationProvider locationProvider, Map<RackStatus, Drawable> markers)
	{
		super(boundCenterBottom(defaultMarker), mapView);
		this.context = mapView.getContext();
		this.locationProvider = locationProvider;
		this.markers = markers;
	}
	
	public void addBikeLocations(List<BikeLocation> newLocations)
	{
		bikeLocations.clear();
        bikeLocations.addAll(newLocations);
		populate();
	}
	
	public void addBikeLocation(BikeLocation newLocation)
	{
		bikeLocations.add(newLocation);
		populate();
	}
	
	@Override
	protected CustomItem createItem(int i) {
		BikeLocation loc = bikeLocations.get(i);
		return new CustomItem(loc.getLocation(), loc.getName(), "", boundCenterBottom(markers.get(loc.getLocationData().getStatus())));
	}
	
	protected BikeLocation createLocationItem(int index)
	{
		return bikeLocations.get(index);
	}
	
	private BikeLocationData getLocationData(Long id)
	{
		return locationProvider.getLocationData(id);
	}

	@Override
	public int size() {
		return bikeLocations.size();
	}
	
	@Override
	protected void setData(final BalloonOverlayView view, int itemIndex) {
//		dialog = ProgressDialog.show(context, "", context.getString(R.string.loadingRackData), true);
		new LocationDataTask().execute(itemIndex);
	}
	
	public void zoomToBikeLocationId(Long id)
	{
		for(int i = 0; i < bikeLocations.size(); i++)
		{
			if(id.equals(bikeLocations.get(i).getId()))
			{
				onTap(i);
				break;
			}
		}
	}
	
	private class LocationDataTask extends AsyncTask<Integer, Void, BikeLocation> {

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if(dialog != null)
			{
				dialog.dismiss();
			}
		}

		@Override
		protected BikeLocation doInBackground(Integer... params) {
			BikeLocation location = createLocationItem(params[0]);
			try {
				BikeLocationData data = getLocationData(location.getId());
				location.setLocationData(data);
				return location;
			}
			catch(Exception e)
			{
				return null;
			}
		}

		@Override
		protected void onPostExecute(BikeLocation result) {
			if(dialog != null)
			{
				dialog.dismiss();
			}
			if(result != null) {
				balloonView.setData(result, null);
				showBubble();
			} else {
                Toast.makeText(context, R.string.errMsgNoData, Toast.LENGTH_LONG).show();
			}
		}
		
	}
}

class CustomItem extends OverlayItem {
	
	Drawable marker = null;


	public CustomItem(GeoPoint point, String title, String snippet, Drawable marker) {
		super(point, title, snippet);
		
		this.marker = marker;
	}
	
	@Override
	public Drawable getMarker(int stateBitset) {
		setState(marker, stateBitset);
		
		return marker;	
	}
}*/
}
