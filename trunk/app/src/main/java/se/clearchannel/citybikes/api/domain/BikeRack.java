package se.clearchannel.citybikes.api.domain;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="station")
public class BikeRack {

	@Element(name="rack_id", required=false)
	private Long id;
	
	@Element(required=false)
	private String description;
	
	@Element
	private Double longitude;
	
	@Element
	private Double latitude;
	
	@Element
	private Integer ready_bikes;
	
	@Element
	private Integer empty_locks;
	
	@Element
	private Integer online;
	
	@Element
	private String last_update;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Integer getReady_bikes() {
		return ready_bikes;
	}

	public void setReady_bikes(Integer ready_bikes) {
		this.ready_bikes = ready_bikes;
	}

	public Integer getEmpty_locks() {
		return empty_locks;
	}

	public void setEmpty_locks(Integer empty_locks) {
		this.empty_locks = empty_locks;
	}

	public Integer getOnline() {
		return online;
	}

	public void setOnline(Integer online) {
		this.online = online;
	}

	public String getLast_update() {
		return last_update;
	}

	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}
	
	
}
