package se.clearchannel.citybikes.api;

import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import se.clearchannel.citybikes.MainActivity;
import se.clearchannel.citybikes.api.domain.BikeRack;
import se.clearchannel.citybikes.api.domain.RackList;
import se.clearchannel.citybikes.api.security.Authentication;

import java.nio.charset.Charset;
import java.util.List;

public class RestApiClient {

    final private static String GET_ALL_RACKS_URL = "https://secure.citybikes.se/services/getracks";
    final private static String GET_RACK_DATA_URL = "https://secure.citybikes.se/services/getrack?rackId={rackId}";
    final private static String REPORT_SERVICE_URL = "https://secure.citybikes.se/services/requestservice";

//    final private static String GET_ALL_RACKS_URL = "http://cbapiv2.appified.net/getracks";
//    final private static String GET_RACK_DATA_URL = "http://cbapiv2.appified.net/getrack?rackId={rackId}";
//    final private static String REPORT_SERVICE_URL = "http://cbapiv2.appified.net/requestservice";


    public RackList getRackList() {

        RestTemplate restTemplate = new RestTemplate();
        SimpleXmlHttpMessageConverter formConverter = new SimpleXmlHttpMessageConverter();

        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        converters.add(formConverter);
        restTemplate.setMessageConverters(converters);
        HttpEntity<?> requestEntity = getRequestEntity();

        RackList rackList;
        try {
            ResponseEntity<RackList> exchange = restTemplate.exchange(GET_ALL_RACKS_URL, HttpMethod.GET, requestEntity, RackList.class);
            rackList = exchange.getBody();
        } catch (Exception e) {
            throw new ApiClientException(e);
        }

        return rackList;
    }

    public BikeRack getBikeRack(Long id) {

        RestTemplate restTemplate = new RestTemplate();
        SimpleXmlHttpMessageConverter formConverter = new SimpleXmlHttpMessageConverter();
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        converters.add(formConverter);
        restTemplate.setMessageConverters(converters);
        HttpEntity<?> requestEntity = getRequestEntity();
        String body = "";
        HttpHeaders headers = getHttpHeaders();
        BikeRack rack;
        try {
            ResponseEntity<BikeRack> responseEntity = restTemplate.exchange(GET_RACK_DATA_URL, HttpMethod.GET, requestEntity, BikeRack.class, id);
            body = responseEntity.toString();
            rack = responseEntity.getBody();
        } catch (Exception e) {
            throw new ApiClientException(e);
        }
        return rack;
    }

    public void sendServiceReport(MultiValueMap<String, Object> formData) {
        RestTemplate restTemplate = new RestTemplate();

        FormHttpMessageConverter formConverter = new FormHttpMessageConverter();
        formConverter.setCharset(Charset.forName("UTF-8"));
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        converters.add(formConverter);
        converters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        restTemplate.setMessageConverters(converters);

//            restTemplate.setErrorHandler(new CustomErrorHandler());

        HttpHeaders requestHeaders = getHttpHeaders();

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(formData, requestHeaders);

        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(REPORT_SERVICE_URL, HttpMethod.POST, requestEntity, String.class);
//                Log.d("SERVICE REPORT", responseEntity.getStatusCode().toString() + " " + responseEntity.getBody());
        } catch (Exception e) {
//                Log.e("SERVICE REPORT", e.getMessage());
            throw new ApiClientException(e);
        }
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setUserAgent(MainActivity.getUserAgent());

        Authentication token = Authentication.getNewToken();
        requestHeaders.set("Auth-Hash", token.getHash());
        requestHeaders.set("Auth-Operator", token.getOperatorId());
        requestHeaders.set("Auth-String", token.getUniqueRequestString());
        return requestHeaders;
    }

    private HttpEntity<?> getRequestEntity() {
        HttpHeaders requestHeaders = getHttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_XML);

        HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
        return requestEntity;
    }

}
