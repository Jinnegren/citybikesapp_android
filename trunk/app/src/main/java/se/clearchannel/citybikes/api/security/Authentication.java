package se.clearchannel.citybikes.api.security;


import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Authentication {

    private final String operatorId = "28ndjn3198b";
    private final String operatorSecret = "Nsiw8)k2!#,kd#2s";
    private final String uniqueRequestString;


    public static Authentication getNewToken() {
        return new Authentication();
    }

    private Authentication() {
        UUID randomId = UUID.randomUUID();
        uniqueRequestString = randomId.toString();
    }

    public String getOperatorId() {
        return operatorId;
    }

    public String getUniqueRequestString() {
        return uniqueRequestString;
    }

    private String getOperatorSecret() {
        return operatorSecret;
    }

    public String getHash() {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        StringBuilder builder = new StringBuilder();
        builder.append(getOperatorId());
        builder.append(getOperatorSecret());
        builder.append(getUniqueRequestString());
        String text = builder.toString();

        try {
            md.update(text.getBytes("UTF-8")); // Change this to "UTF-16" if needed
        } catch (UnsupportedEncodingException e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        byte[] digest = md.digest();

        BigInteger bigInt = new BigInteger(1, digest);
        String output = bigInt.toString(16);
        return output;
    }
}
