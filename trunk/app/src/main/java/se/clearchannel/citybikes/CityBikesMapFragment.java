package se.clearchannel.citybikes;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.location.LocationListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.clearchannel.citybikes.api.FileHandler;
import se.clearchannel.citybikes.locations.BikeLocation;
import se.clearchannel.citybikes.locations.BikeLocationData;
import se.clearchannel.citybikes.locations.LocationProvider;


/**
 * Created by perj on 2014-03-20.
 */
public class CityBikesMapFragment extends Fragment implements com.google.android.gms.location.LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, RefreshListener {

    private boolean myLocationEnabled = true;
    private LocationProvider locationProvider;
    private boolean locationsUpToDate = false;
    private HashMap<Marker, BikeLocation> stationMarkerMap;
    ProgressDialog dialog;
    LocationsUpdaterTask updateTask;
    private final int GROUP_DEFAULT = 0, GROUP_MAP_LAYERS = 1, GROUP_OFFLINE_MAP = 3;
    private final int MENU_MY_LOC = 0, MENU_MAP_LAYERS = 1, MENU_REFRESH_MAP = 2, MENU_OFFLINE_MAP = 3;
    private final int SUB_MENU_LAYERS_MAP = 101, SUB_MENU_LAYERS_SAT = 102;
    private final int SUB_MENU_OFFLINE_UPPSALA = 301, SUB_MENU_OFFLINE_STOCKHOLM = 302;
    LocationManager locationManager;

    public final int ID_DIALOG_PROGRESS = 1001;

    private static View view;
    /**
     * Note that this may be null if the Google Play services APK is not
     * available.
     */
    LocationRequest mLocationRequest;
    LocationClient mLocationClient;

    private static GoogleMap mMap;
    private static Double latitude, longitude;
    private CharSequence[] mCities;

    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        super.onCreateView(inflater, container, savedInstanceState);
        if (container == null) {
            return null;
        }
        view = (RelativeLayout) inflater.inflate(R.layout.map_fragment, container, false);
        mLocationRequest = LocationRequest.create();
        mLocationClient = new LocationClient(getActivity(),this, this);       // Use high accuracy
        mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
       setUpMapIfNeeded();// For setting up the MapFragment
        this.setHasOptionsMenu(true);
        //mapFragment.setHasOptionsMenu(true);
        //((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).setHasOptionsMenu(true);
        mMap.setMyLocationEnabled(true);
        myLocationEnabled = mMap.isMyLocationEnabled();

        stationMarkerMap = new HashMap<Marker, BikeLocation>();
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LocationDataTask task = new LocationDataTask();
                task.execute(marker);
                return true;
            }
        });

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {



            private final View contents = getActivity().getLayoutInflater().inflate(R.layout.balloon_overlay, null);



            @Override

            public View getInfoWindow(Marker marker) {

                //Only changing the content for this tutorial

                //if you return null, it will just use the default window
                BikeLocation location = stationMarkerMap.get(marker);





                TextView title = (TextView) contents.findViewById(R.id.balloon_item_title);
                TextView availableBikes = (TextView) contents.findViewById(R.id.location_noOfBikes);
                TextView availableSlots = (TextView) contents.findViewById(R.id.location_noOfFree);

                title.setText(location.getName());

                availableBikes.setText(Integer.toString(location.getLocationData().getAvailableBikes()));
                availableSlots.setText(Integer.toString(location.getLocationData().getFreeSlots()));

                return contents;

            }



            @Override

            public View getInfoContents(Marker marker) {


                return null;


            }



        });
        showCurrentLocation();

        mCities = FileHandler.getCities();

       return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        mMap.setMyLocationEnabled(false);
        if (mLocationClient.isConnected()) {
            /*
             * Remove location updates for a listener.
             * The current Activity is the listener, so
             * the argument is "this".
             */
            mLocationClient.removeLocationUpdates(this);
        }
        /*
         * After disconnect() is called, the client is
         * considered "dead".
         */
        mLocationClient.disconnect();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mLocationClient.connect();
        if(dialog != null)
        {
            dialog.dismiss();
            dialog = null;
        }
        if(myLocationEnabled) {
            mMap.setMyLocationEnabled(true);
        }
        if(!locationsUpToDate) {
            createUpdateTask();
        }
        if(!connected())
        {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                            builder.setTitle(R.string.choose_city)
                                    .setItems(mCities, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            openPDF(mCities[which].toString());
                                        }
                                    }).setCancelable(true).show();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


            builder.setMessage(getString(R.string.offline_message)).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {

        SubMenu mapviewMenu = menu.addSubMenu(GROUP_MAP_LAYERS, MENU_MAP_LAYERS, 0, R.string.mapView);
        mapviewMenu.setIcon(R.drawable.ic_action_map);
        mapviewMenu.add(GROUP_MAP_LAYERS, SUB_MENU_LAYERS_MAP, 0, R.string.layerMap).setChecked(!(mMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE));
        mapviewMenu.add(GROUP_MAP_LAYERS, SUB_MENU_LAYERS_SAT, 1, R.string.layerSat).setChecked(mMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE);
        mapviewMenu.setGroupCheckable(GROUP_MAP_LAYERS, true, true);
        MenuItemCompat.setShowAsAction(mapviewMenu.getItem(), MenuItem.SHOW_AS_ACTION_IF_ROOM);
        MenuItem refreshMap = menu.add(GROUP_DEFAULT, MENU_REFRESH_MAP, 0, R.string.refreshMap);

        refreshMap.setIcon(R.drawable.ic_action_refresh);
        if(FileHandler.offLineMapExist(getActivity(), "Uppsala") && FileHandler.offLineMapExist(getActivity(), "Stockholm"))
                {
                    SubMenu offlineMapMenu = menu.addSubMenu(GROUP_OFFLINE_MAP, MENU_OFFLINE_MAP, 0, R.string.offline_maps);
                    offlineMapMenu.setIcon(R.drawable.ic_action_map);
                    offlineMapMenu.add(GROUP_OFFLINE_MAP, SUB_MENU_OFFLINE_UPPSALA, 0, "Uppsala");
                    offlineMapMenu.add(GROUP_OFFLINE_MAP, SUB_MENU_OFFLINE_STOCKHOLM, 1, "Stockholm");
                }
                MenuItemCompat.setShowAsAction(refreshMap,MenuItem.SHOW_AS_ACTION_IF_ROOM);

                super.onCreateOptionsMenu(menu, inflater);
            }

            @Override
            public boolean onOptionsItemSelected(MenuItem item)
            {
                if(item.getItemId() == MENU_MY_LOC)
                {
                    showCurrentLocation();
                    int locButtonText = myLocationEnabled ? R.string.hideMyLocation : R.string.myLocation;
                    item.setTitle(locButtonText);
                    return true;
                }
                else if(item.getItemId() == SUB_MENU_LAYERS_MAP)
                {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    item.setChecked(true);
                    return true;
                }
                else if(item.getItemId() == SUB_MENU_LAYERS_SAT)
                {
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    item.setChecked(true);
                    return true;
                }
                else if(item.getItemId() == MENU_REFRESH_MAP)
                {
                    if(!connected())
                    {
                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case DialogInterface.BUTTON_POSITIVE:
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                                        builder.setTitle(R.string.choose_city)
                                                .setItems(mCities, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        openPDF(mCities[which].toString());
                                                    }
                                                }).setCancelable(true).show();
                                        break;

                                    case DialogInterface.BUTTON_NEGATIVE:
                                        //No button clicked
                                        break;
                                }
                            }
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


                        builder.setMessage(getString(R.string.offline_message)).setPositiveButton("Yes", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();
                    }
                    else
                    {
                        createUpdateTask();
                    }
                }
                    else if(item.getItemId() == SUB_MENU_OFFLINE_UPPSALA || item.getItemId() ==SUB_MENU_OFFLINE_STOCKHOLM)
                    {

                        openPDF(item.getTitle().toString());


                     }

        return super.onOptionsItemSelected(item);
    }

    /***** Sets up the map if it is possible to do so *****/
    public void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getActivity().getSupportFragmentManager()
                    .findFragmentById(R.id.location_map)).getMap();
            // Check if we were successful in obtaining the map.
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment)getActivity().getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.

        }
    }

    /**** The mapfragment's id must be removed from the FragmentManager
     **** or else if the same it is passed on the next time then
     **** app will crash ****/
    @Override
    public void onDestroyView() {
        super.onDestroyView();



    }
    public void destroyView(){
        if (mMap != null) {
            MainActivity.fragmentManager.beginTransaction()
                    .remove(MainActivity.fragmentManager.findFragmentById(R.id.location_map)).commitAllowingStateLoss();
            mMap = null;
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();
    }

    public void showCurrentLocation()
    {
        if(!myLocationEnabled)
        {
            mMap.setMyLocationEnabled(false);
            myLocationEnabled = false;
        }
        else
        {
            mMap.setMyLocationEnabled(true);

            myLocationEnabled = true;
        }
        zoomToMyLocation();
    }


    protected boolean isRouteDisplayed() {
        return false;
    }

    private void zoomToMyLocation() {
        LocationManager locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null)
        {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(13)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
        else
        {

        }
    }
    private void zoomToMyLocation(Location location) {
        LocationManager locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();


        if (location != null)
        {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(13)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
        else
        {

        }
    }
    public void createUpdateTask() {
        dialog = ProgressDialog.show(getActivity(), "", getString(R.string.loadingRackData), true, true, new DialogInterface.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if(updateTask != null)
                {
                    updateTask.cancel(true);
                }
            }
        });
        updateTask = new LocationsUpdaterTask();
        updateTask.execute();
    }

    private void updateAllLocations() {
        locationProvider = LocationProvider.getInstance();
        Drawable mapMarker = getActivity().getResources().getDrawable(R.drawable.pin30_green);
        Map<BikeLocationData.RackStatus, Drawable> markers = new HashMap<BikeLocationData.RackStatus, Drawable>();
        markers.put(BikeLocationData.RackStatus.ONLINE_OK, getResources().getDrawable(R.drawable.pin30_green));
        markers.put(BikeLocationData.RackStatus.ONLINE_FULL, getResources().getDrawable(R.drawable.pin30_yellow));
        markers.put(BikeLocationData.RackStatus.ONLINE_EMPTY, getResources().getDrawable(R.drawable.pin30_blue));
        markers.put(BikeLocationData.RackStatus.OFFLINE, getResources().getDrawable(R.drawable.pin30_red));
        //locationsOverlay = new LocationsOverlay(mapMarker, mapFragment, locationProvider, markers);
        mMap.clear();
        List<BikeLocation> bikeLocations =locationProvider.getLocations();

        for (int i = 0; i < bikeLocations.size(); i++) {

            BitmapDescriptor bitmapMarker;
            switch (bikeLocations.get(i).getLocationData().getStatus())
            {
                case ONLINE_OK:
                    bitmapMarker = BitmapDescriptorFactory.fromResource(R.drawable.pin30_green);
                    break;
                case ONLINE_FULL:
                    bitmapMarker = BitmapDescriptorFactory.fromResource(R.drawable.pin30_yellow);
                    break;
                case ONLINE_EMPTY:
                    bitmapMarker = BitmapDescriptorFactory.fromResource(R.drawable.pin30_blue);
                    break;
                case OFFLINE:
                    bitmapMarker = BitmapDescriptorFactory.fromResource(R.drawable.pin30_red);
                    break;
                default:
                    bitmapMarker = BitmapDescriptorFactory.fromResource(R.drawable.pin30_red);
                    break;
            }
            Marker marker = mMap.addMarker(new MarkerOptions().position(bikeLocations.get(i).getLocation()).title(bikeLocations.get(i).getName())
                    .icon(bitmapMarker));
            stationMarkerMap.put(marker, bikeLocations.get(i));


        }

        //locationsUpToDate = true;
    }

    private void goTo(BikeLocation bl)
    {



        if (bl != null)
        {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(bl.getLocation(), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(bl.getLocation())      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    public void zoomToBikelocation(BikeLocation bl)
    {
        goTo(bl);
    }

    @Override
    public void onRefresh(MainActivity activity) {

    }


    public void onLocationChanged(Location location) {
        if(location != null)
        {
            zoomToMyLocation(location);
            if (mLocationClient.isConnected()) {
            /*
             * Remove location updates for a listener.
             * The current Activity is the listener, so
             * the argument is "this".
             */
                mLocationClient.removeLocationUpdates(this);
            }

        }

    }
    private void requestAllUpdates() {
        List<String> providers = locationManager.getAllProviders();
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }




    @Override
    public void onConnected(Bundle bundle) {
        requestAllUpdates();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class LocationsUpdaterTask extends AsyncTask<Void, Void, Integer> {

        private final Integer LOCATIONS_RESULT_SUCCESS = 1;
        private final Integer LOCATIONS_RESULT_ERROR = 0;

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if(dialog != null)
            {
                dialog.dismiss();
                dialog = null;
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            List<BikeLocation> locationsFromServer = LocationProvider.getInstance().getLocationsFromServer();
            if(locationsFromServer.size() > 0) {
                return LOCATIONS_RESULT_SUCCESS;
            } else {
                return LOCATIONS_RESULT_ERROR;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(dialog != null)
            {
                dialog.dismiss();
                dialog = null;
            }
            if(result == LOCATIONS_RESULT_SUCCESS) {
                updateAllLocations();
            } else if(result == LOCATIONS_RESULT_ERROR) {
                Toast.makeText(getActivity(), R.string.errMsgNoData, Toast.LENGTH_LONG).show();
            }
        }
    }

    private class LocationDataTask extends AsyncTask<Marker, Void, Marker> {

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if(dialog != null)
            {
                dialog.dismiss();
                dialog = null;
            }
        }

        @Override
        protected Marker doInBackground(Marker... params) {
            Marker marker = params[0];
            BikeLocation location = stationMarkerMap.get(marker);
            try {
                BikeLocationData data = locationProvider.getLocationDataFromServer(location.getId());
                location.setLocationData(data);
                stationMarkerMap.remove(marker);
                stationMarkerMap.put(marker, location);
                return marker;
            }
            catch(Exception e)
            {
                Exception ex = e;
                return null;
            }
        }

        @Override
        protected void onPostExecute(Marker result) {
            if(dialog != null)
            {
                dialog.dismiss();
                dialog = null;
            }
            if(result != null) {

                result.showInfoWindow();

                zoomToBikelocation(stationMarkerMap.get(result));
            } else {
                Toast.makeText(getActivity(), R.string.errMsgNoData, Toast.LENGTH_LONG).show();
            }
        }

    }


    private boolean connected()
    {
        ConnectivityManager cm =
                (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public void openPDF(String city)
    {
        File file = FileHandler.getFile(getActivity(), city);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
        }
    }


}

