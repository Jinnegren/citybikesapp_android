package se.clearchannel.citybikes.locations;

import com.google.android.gms.maps.model.LatLng;
import se.clearchannel.citybikes.api.RestApiClient;
import se.clearchannel.citybikes.api.domain.BikeRack;
import se.clearchannel.citybikes.api.domain.RackList;

import java.util.ArrayList;
import java.util.List;

public class LocationProvider {
	
	private static LocationProvider instance = new LocationProvider();

    private RestApiClient apiClient = new RestApiClient();
	
	private List<BikeLocation> locations =  new ArrayList<BikeLocation>();

	private LocationProvider() { }
	
	public static LocationProvider getInstance()
	{
		return instance;
	}
	
	public List<BikeLocation> getLocations() {
		return locations;
	}
	
	public List<BikeLocation> getLocationsFromServer()
	{

        try {
            RackList rackList = apiClient.getRackList();
            locations = convertToLocations(rackList);
        } catch (Exception e) {
            //do nothing
        }

		this.locations = locations;
		return locations;
	}
	
	private List<BikeLocation> convertToLocations(RackList racks) {
        List<BikeLocation> locations = new ArrayList<BikeLocation>();
		for(BikeRack rack : racks.getRacks())
		{
            if(rack.getLatitude() == 0 && rack.getLongitude() == 0) {
                continue;
            }
			LatLng point = new LatLng(rack.getLatitude(), rack.getLongitude());
			BikeLocation location = new BikeLocation(rack.getId(), rack.getDescription(), point);
			BikeLocationData locData = new BikeLocationData();
			locData.setAvailableBikes(rack.getReady_bikes());
			locData.setFreeSlots(rack.getEmpty_locks());
			locData.setId(rack.getId());
			locData.setOnline((rack.getOnline()==1));
			location.setLocationData(locData);
			locations.add(location);
		}
        return locations;
	}

	public BikeLocationData getLocationData(Long id) {
        for(BikeLocation location : locations) {
            if(location.getId().equals(id)) {
                return location.getLocationData();
            }
        }
        return null;
	}

    public BikeLocationData getLocationDataFromServer(Long id) {
        BikeRack rack = apiClient.getBikeRack(id);

        BikeLocationData locationData = new BikeLocationData();
        locationData.setId(id);
        if(rack.getOnline() > 0) {
            locationData.setAvailableBikes(rack.getReady_bikes());
            locationData.setFreeSlots(rack.getEmpty_locks());
        } else {
            locationData.setAvailableBikes(0);
            locationData.setFreeSlots(0);
        }
        return locationData;
    }
    public void resetLocations()
    {
        this.locations = new ArrayList<>();
    }
}
