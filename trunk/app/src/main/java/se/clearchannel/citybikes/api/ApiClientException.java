package se.clearchannel.citybikes.api;

public class ApiClientException extends RuntimeException {

    public ApiClientException(Throwable throwable) {
        super(throwable);
    }
}
