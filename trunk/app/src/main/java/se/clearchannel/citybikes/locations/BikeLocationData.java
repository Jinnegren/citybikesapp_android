package se.clearchannel.citybikes.locations;

public class BikeLocationData {
	
	public static enum RackStatus {
		ONLINE_OK, ONLINE_FULL, ONLINE_EMPTY, OFFLINE
	}

	private Long id;
	private int totalSlots;
	private int freeSlots;
	private int availableBikes;
	private boolean online;
	
	public void setId(Long id)
	{
		this.id = id;
	}
	public Long getId()
	{
		return id;
	}
	public int getTotalSlots() {
		return totalSlots;
	}
	public void setTotalSlots(int totalSlots) {
		this.totalSlots = totalSlots;
	}
	public int getFreeSlots() {
		return freeSlots;
	}
	public void setFreeSlots(int freeSlots) {
		this.freeSlots = freeSlots;
	}
	public int getAvailableBikes() {
		return availableBikes;
	}
	public void setAvailableBikes(int availableBikes) {
		this.availableBikes = availableBikes;
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online){
		this.online = online;
	}
	
	public RackStatus getStatus() {
		if(!online){
			return RackStatus.OFFLINE;
		}
		if(availableBikes == 0){
			return RackStatus.ONLINE_EMPTY;
		}
		if(freeSlots == 0) {
			return RackStatus.ONLINE_FULL;
		}
		return RackStatus.ONLINE_OK;
	}
	
	
}
