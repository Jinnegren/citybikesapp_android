package se.clearchannel.citybikes;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.location.LocationListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import se.clearchannel.citybikes.locations.BikeLocation;
import se.clearchannel.citybikes.locations.LocationProvider;

/**
 * Created by perj on 2014-03-26.
 */
public class StationListFragment extends ListFragment implements com.google.android.gms.location.LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, RefreshListener{

    private RackAdapter adapter;
    List<BikeLocation> locations = new ArrayList<BikeLocation>();
    LocationManager locationManager;
    Location myLocation;
    LocationRequest mLocationRequest;
    LocationClient mLocationClient;



    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.list_view_layout, container, false);

        mLocationRequest = LocationRequest.create();
        mLocationClient = new LocationClient(getActivity(),this, this);       // Use high accuracy
        mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        return mainView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);



        locations = LocationProvider.getInstance().getLocations();
        adapter = new RackAdapter(getActivity(), R.layout.list_item_rack, locations);
        setListAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {


            public void onItemClick(AdapterView arg0, View arg1, int arg2,
                                    long arg3) {

                BikeLocation bl = (BikeLocation) getListView().getItemAtPosition(arg2);
                MainActivity activity = (MainActivity)getActivity();
                activity.switchToMapTab(bl);

            }
        });

    }

    private void requestAllUpdates() {

        List<String> providers = locationManager.getAllProviders();
        for(String prov : providers)
        {
            mLocationClient.requestLocationUpdates(mLocationRequest, this);
        }
    }

    @Override
    public void onRefresh(MainActivity activity) {
        locations = LocationProvider.getInstance().getLocations();

        adapter = new RackAdapter(getActivity(), R.layout.list_item_rack, locations);
        setListAdapter(adapter);


        //requestAllUpdates();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onConnected(Bundle bundle) {
        requestAllUpdates();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class RackAdapter extends ArrayAdapter<BikeLocation> {

        private List<BikeLocation> items;

        public RackAdapter(Context context, int textViewResourceId,
                           List<BikeLocation> objects) {
            super(context, textViewResourceId, objects);
            this.items = objects;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if(v == null) {
                LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.list_item_rack, null);
            }
            BikeLocation location = items.get(position);
            if(location != null)
            {
                TextView description = (TextView) v.findViewById(R.id.list_item_rack_description);
                if(description != null)
                {
                    description.setText(location.getName());
                }

                TextView distance = (TextView) v.findViewById(R.id.list_item_rack_distance);
                if(distance != null && location.getDistanceToMyLocation() != null)
                {
                    float dist = location.getDistanceToMyLocation();
                    distance.setText(distanceToString(dist));
                }

                TextView bikes = (TextView) v.findViewById(R.id.list_item_rack_available_bikes);
                if(bikes != null) {
                    bikes.setText(Integer.toString(location.getLocationData().getAvailableBikes()));
                }

                TextView slots = (TextView) v.findViewById(R.id.list_item_rack_empty_slots);
                if(slots != null) {
                    slots.setText(Integer.toString(location.getLocationData().getFreeSlots()));
                }

            }

            return v;
        }



    }

    private String distanceToString(float distance)
    {

        if(distance <= 1000)
        {
            String s = Long.toString(Math.round(distance));
            return s + "m";
        }
        else
        {
            String s = Long.toString(Math.round(distance / 1000.0));
            return s + "km";
        }
    }

    public void onLocationChanged(Location location) {
        if(location != null)
        {
            myLocation = location;

            Location dummyLocation = new Location(myLocation);
            LatLng point;
            for(BikeLocation bl : locations)
            {
                point = bl.getLocation();
                dummyLocation.setLatitude(point.latitude);
                dummyLocation.setLongitude(point.longitude);
                float dist = myLocation.distanceTo(dummyLocation);
                bl.setDistanceToMyLocation(dist);
            }
            Collections.sort(locations, new SortableByDistance());
            adapter.notifyDataSetChanged();
        }
    }

    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }


    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }


    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mLocationClient.isConnected()) {
            /*
             * Remove location updates for a listener.
             * The current Activity is the listener, so
             * the argument is "this".
             */
            mLocationClient.removeLocationUpdates(this);
        }
        /*
         * After disconnect() is called, the client is
         * considered "dead".
         */
        mLocationClient.disconnect();

    }

    @Override
    public void onResume() {
        super.onResume();
        mLocationClient.connect();
        locations = LocationProvider.getInstance().getLocations();

        adapter = new RackAdapter(getActivity(), R.layout.list_item_rack, locations);
        setListAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {


            public void onItemClick(AdapterView arg0, View arg1, int arg2,
                                    long arg3) {

                BikeLocation bl = (BikeLocation) getListView().getItemAtPosition(arg2);
                MainActivity activity = (MainActivity)getActivity();
                activity.switchToMapTab(bl);

            }
        });

        adapter.notifyDataSetChanged();
    }

    private class SortableByDistance implements Comparator<BikeLocation> {

        public int compare(BikeLocation object1, BikeLocation object2) {
            if(object1.getDistanceToMyLocation() == null) {
                return -1;
            }
            if(object2.getDistanceToMyLocation() == null) {
                return 1;
            }
            return object1.getDistanceToMyLocation().compareTo(object2.getDistanceToMyLocation());

        }

    }

}
