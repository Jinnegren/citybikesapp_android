package se.clearchannel.citybikes;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

import org.springframework.core.io.FileSystemResource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import se.clearchannel.citybikes.api.RestApiClient;
import se.clearchannel.citybikes.locations.BikeLocation;
import se.clearchannel.citybikes.locations.LocationProvider;

/**
 * Created by perj on 2014-03-26.
 */
public class ServiceFragment extends Fragment implements com.google.android.gms.location.LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, View.OnTouchListener, RefreshListener{
    ProgressDialog dialog;

    ArrayAdapter<BikeLocation> adapter;
    private List<BikeLocation> locations;
    private static final long TIME_BETWEEN_REPORTS = 7200000;

    private Spinner serviceLocationSpinner;
    private Button lockNumberButton;
    private EditText otherInfo;
    private Button selectButton;

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Uri fileUri;

    private LocationManager locationManager;
    private Location myLocation;

    private List<Integer> selectedItems = new ArrayList<Integer>();
    boolean[] checkedItems = new boolean[10];
    private String selectedItemsContent = "";
    private String selectedLockNumber = "";

    private static final int PROBLEM_DIALOG = 0;
    private static final int LOCK_NUMBER_DIALOG = 1;

    private void resetForm() {
        selectedItems.clear();
        updateSelectedItems();
        selectedLockNumber = "";
        serviceLocationSpinner.setSelection(0);
        fileUri = null;
        otherInfo.setText("");
    }


    LocationRequest mLocationRequest;
    LocationClient mLocationClient;



    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        View mainView = (View) inflater.inflate(R.layout.service_select_location, container, false);

        ImageView iv = (ImageView) mainView.findViewById (R.id.bike_image_default);
        if (iv != null) {
            iv.setOnTouchListener (this);
        }
        mLocationRequest = LocationRequest.create();
        mLocationClient = new LocationClient(getActivity(),this, this);       // Use high accuracy
        mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        return mainView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locations = LocationProvider.getInstance().getLocations();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        View mainView = this.getView();
        serviceLocationSpinner = (Spinner)mainView.findViewById(R.id.service_locations_spinner);
        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, locations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        serviceLocationSpinner.setAdapter(adapter);

        lockNumberButton = (Button) mainView.findViewById(R.id.service_lock_number);
        lockNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CBDialogFragment dialogFragment = new CBDialogFragment(LOCK_NUMBER_DIALOG);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "HEJ");
            }
        });
        otherInfo = (EditText) mainView.findViewById(R.id.service_other_info_text);


        Button photoButton = (Button) mainView.findViewById(R.id.service_photo_button);
        if(checkCameraHardware(getActivity())) {
            photoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // create Intent to take a picture and return control to the calling application
                    if(!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                        Toast.makeText(getActivity(), R.string.insert_sd_card, Toast.LENGTH_LONG).show();
                        return;
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
                    getActivity().setResult(getActivity().RESULT_OK, intent);

                    // start the image capture Intent
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
            });
        } else {
            photoButton.setVisibility(View.GONE);
        }

        selectButton = (Button) mainView.findViewById(R.id.select_problem);
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CBDialogFragment dialogFragment = new CBDialogFragment(PROBLEM_DIALOG);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "HEJ");

            }
        });
        Button submitButton = (Button)mainView.findViewById(R.id.submit_report_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String selectedLocation = serviceLocationSpinner.getSelectedItem().toString();

                if(selectedLocation == null || selectedItemsNotSet() || selectedLockNumberNotSet()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.submit_error_missing_fields)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else {
                    SharedPreferences sharedPref = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                    long lastReportTime = sharedPref.getLong(getString(R.string.preference_last_report_time_key), 0);
                    int numberOfReports = sharedPref.getInt(getString(R.string.preference_number_of_reports_key), 0);
                    long currentTime = System.currentTimeMillis();


                    if((currentTime - TIME_BETWEEN_REPORTS) > lastReportTime)
                    {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(getString(R.string.preference_number_of_reports_key), 0);
                        editor.commit();
                        dialog = ProgressDialog.show(getActivity(), "", getActivity().getString(R.string.service_sending), true);
                        new ServiceReportTask().execute();
                    }
                    else if(numberOfReports < 3)
                    {
                        dialog = ProgressDialog.show(getActivity(), "", getActivity().getString(R.string.service_sending), true);
                        new ServiceReportTask().execute();
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(R.string.reports_number_exceeded_title)
                                .setTitle(R.string.submit_error_done_title)
                                .setCancelable(true)
                                .setPositiveButton("OK", null);
                        builder.create();
                    }
                }
            }
        });
    }

    private boolean selectedLockNumberNotSet() {
        return selectedLockNumber == null || selectedLockNumber.length() == 0;
    }

    private boolean selectedItemsNotSet() {
        return selectedItemsContent == null || selectedItemsContent.length() == 0;
    }

    //@Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        if(id == PROBLEM_DIALOG) {
            ListView lv = ((AlertDialog) dialog).getListView();

            for (int i=0; i<checkedItems.length; i++){
                lv.setItemChecked(i, checkedItems[i]);
            }
        }

    }

    //@Override
    public Dialog onCreateDialog( int id )
    {
        if(id == PROBLEM_DIALOG) {

            return new AlertDialog.Builder( getActivity() )
                    .setTitle(R.string.select_error_label)
                    .setMultiChoiceItems(R.array.error_report_items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which,
                                            boolean isChecked) {
                            if (isChecked) {
                                // If the user checked the item, add it to the selected items

                                selectedItems.add(which);
                            } else if (selectedItems.contains(which)) {
                                // Else, if the item is already in the array, remove it
                                selectedItems.remove(Integer.valueOf(which));
                            }
                            updateSelectedItems();

                        }
                    })
                    .setPositiveButton("OK", new DialogButtonClickHandler())
                    .create();
        } else if(id == LOCK_NUMBER_DIALOG) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.service_lock_number_text);
            builder.setItems(R.array.service_lock_numbers, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    selectedLockNumber = Integer.toString(which+1);
                }
            });
            return builder.create();
        }
        return null;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final int action = motionEvent.getAction();
        final int evX = (int) motionEvent.getX();
        final int evY = (int) motionEvent.getY();

        switch (action) {
            case MotionEvent.ACTION_UP:

                int touchColor = getHotspotColor (R.id.bike_hotspot, evX, evY);

                ColorTool ct = new ColorTool ();
                int tolerance = 25;

                if (ct.closeMatch (BT_CHAIN, touchColor, tolerance)) {
                    addOrRemoveSelected(getIndexOfItem(getResources().getString(R.string.item_chain)));
                } else if(ct.closeMatch(BT_FRONT_TYRE, touchColor, tolerance)) {
                    addOrRemoveSelected(getIndexOfItem(getResources().getString(R.string.item_frontTyre)));
                } else if(ct.closeMatch(BT_HANDLE_BARS, touchColor, tolerance)) {
                    addOrRemoveSelected(getIndexOfItem(getResources().getString(R.string.item_handlebars)));
                } else if(ct.closeMatch(BT_LIGHTS, touchColor, tolerance)) {
                    addOrRemoveSelected(getIndexOfItem(getResources().getString(R.string.item_lights)));
                } else if(ct.closeMatch(BT_PEDALS, touchColor, tolerance)) {
                    addOrRemoveSelected(getIndexOfItem(getResources().getString(R.string.item_pedal)));
                } else if(ct.closeMatch(BT_SEAT, touchColor, tolerance)) {
                    addOrRemoveSelected(getIndexOfItem(getResources().getString(R.string.item_seat)));
                } else if(ct.closeMatch(BT_REAR_TYRE, touchColor, tolerance)) {
                    addOrRemoveSelected(getIndexOfItem(getResources().getString(R.string.item_rearTyre )));
                }
        }
        updateSelectedItems();
        return true;
    }

    private int getIndexOfItem(String itemName) {
        String[] errorItems = getResources().getStringArray(R.array.error_report_items);

        int selectedItem = 0;
        for(int i= 0; i < errorItems.length; i++) {
            if(errorItems[i].equals(itemName)) {
                selectedItem = i;
            }
        }
        return  selectedItem;
    }

    private void addOrRemoveSelected(Integer index) {
        if (selectedItems.contains(index)) {
            // Else, if the item is already in the array, remove it
            selectedItems.remove(Integer.valueOf(index));
        } else {
            selectedItems.add(index);
        }
    }

    public int getHotspotColor (int hotspotId, int x, int y) {
        ImageView img = (ImageView) getActivity().findViewById(hotspotId);
        img.setDrawingCacheEnabled(true);
        Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
        img.setDrawingCacheEnabled(false);
        return hotspots.getPixel(x, y);
    }

    @Override
    public void onRefresh(MainActivity activity) {
        //requestAllUpdates();
    }

    @Override
    public void onConnected(Bundle bundle) {
requestAllUpdates();
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public class DialogButtonClickHandler implements DialogInterface.OnClickListener
    {
        public void onClick( DialogInterface dialog, int clicked ) {
            switch( clicked ) {
                case DialogInterface.BUTTON_POSITIVE:
                    updateSelectedItems();
                    break;
            }
        }
    }
    protected void updateSelectedItems()
    {
        String[] errorItems = getResources().getStringArray(R.array.error_report_items);

        Collections.sort(selectedItems);
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(int i = 0; i < errorItems.length; i++) {
            if(selectedItems.contains(i)) {
                if (first)
                    first = false;
                else
                    sb.append(",");
                sb.append(errorItems[i]);

            }
        }
        selectedItemsContent = sb.toString();

        String buttonLabel = "";
        if(selectedItems.size() > 2) {
            buttonLabel = errorItems[selectedItems.get(0)] + " + " + (selectedItems.size() - 1);
        } else if(selectedItems.size() > 0) {
            for(Integer i : selectedItems) {
                buttonLabel += errorItems[i] + ",";
            }
            buttonLabel = buttonLabel.substring(0, buttonLabel.length() -1);
        } else {
            buttonLabel = getResources().getString(R.string.select_error_label);
        }
        selectButton.setText(buttonLabel);

        for(int i = 0; i < checkedItems.length; i++) {
            checkedItems[i] = selectedItems.contains(i);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent

                Toast.makeText(getActivity(), "Image saved to:\n" +
                        fileUri.getPath(), Toast.LENGTH_LONG).show();
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
    }

    private void requestAllUpdates() {
        List<String> providers = locationManager.getAllProviders();
        for(String prov : providers)
        {
            mLocationClient.requestLocationUpdates(mLocationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null)
        {
            myLocation = location;

            Location dummyLocation = new Location(myLocation);
            LatLng point;
            for(BikeLocation bl : locations)
            {
                point = bl.getLocation();
                dummyLocation.setLatitude(point.latitude);
                dummyLocation.setLongitude(point.longitude);
                float dist = myLocation.distanceTo(dummyLocation);
                bl.setDistanceToMyLocation(dist);
            }
            Collections.sort(locations, new SortableByDistance());
            adapter.notifyDataSetChanged();
        }
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "StockholmCityBikes");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
//                Log.d("StockholmCityBikes", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mLocationClient.isConnected()) {
            /*
             * Remove location updates for a listener.
             * The current Activity is the listener, so
             * the argument is "this".
             */
            mLocationClient.removeLocationUpdates(this);
        }
        /*
         * After disconnect() is called, the client is
         * considered "dead".
         */
        mLocationClient.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        mLocationClient.connect();
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            //System.out.print("HAS CAMERA");
            return true;
        } else {
            // no camera on this device
            //System.out.print("NO CAMERA");
            return false;
        }
    }

    private class SortableByDistance implements Comparator<BikeLocation> {

        @Override
        public int compare(BikeLocation object1, BikeLocation object2) {
            if(object1.getDistanceToMyLocation() == null) {
                return -1;
            }
            if(object2.getDistanceToMyLocation() == null) {
                return 1;
            }
            return object1.getDistanceToMyLocation().compareTo(object2.getDistanceToMyLocation());

        }
    }

    private final static int BT_REAR_TYRE = Color.parseColor("#B0B0B0");
    private final static int BT_CHAIN = Color.parseColor("#00FFFF");
    private final static int BT_PEDALS = Color.parseColor("#007FFF");
    private final static int BT_FRONT_TYRE = Color.parseColor("#7FFF00");
    private final static int BT_LIGHTS = Color.parseColor("#FFFF00");
    private final static int BT_HANDLE_BARS = Color.parseColor("#000000");
    private final static int BT_SEAT = Color.parseColor("#FF0000");

    public class ServiceReportTask extends AsyncTask<Void, Void, Integer> {

        private final Integer SUCCESS = 1;
        private final Integer ERROR = 0;

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if(dialog != null)
            {
                dialog.dismiss();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            MultiValueMap<String, Object> formData = new LinkedMultiValueMap<String, Object>();
            formData.add("problem", selectedItemsContent);
            formData.add("station", serviceLocationSpinner.getSelectedItem().toString());
            formData.add("lock", selectedLockNumber);

            String info = otherInfo.getText().toString();
            if(info != null && info.length() > 0) {
                formData.add("information", info);
            }
            if(fileUri != null){
                formData.add("picture", new FileSystemResource(fileUri.getPath()));
            }

            try {
                RestApiClient restApiClient = new RestApiClient();
                restApiClient.sendServiceReport(formData);
                return SUCCESS;
            } catch (Exception e) {
                return ERROR;
            }

        }

        @Override
        protected void onPostExecute(Integer result) {
            if(dialog != null)
            {
                dialog.dismiss();
            }
            if(result == SUCCESS) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.submit_error_done_message)
                        .setTitle(R.string.submit_error_done_title)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                resetForm();
                            }
                        });
                AlertDialog alert = builder.create();
                SharedPreferences sharedPref = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                int numberOfReports = sharedPref.getInt(getString(R.string.preference_number_of_reports_key), 0);
                SharedPreferences.Editor editor = sharedPref.edit();
                if(numberOfReports == 0)
                {
                    editor.putLong(getString(R.string.preference_last_report_time_key), System.currentTimeMillis());
                }
                numberOfReports = numberOfReports + 1;



                editor.putInt(getString(R.string.preference_number_of_reports_key), numberOfReports);
                editor.commit();
                alert.show();
            } else {
                Toast.makeText(getActivity(), R.string.errMsgNoData, Toast.LENGTH_LONG).show();
            }

        }
    }
    public class CBDialogFragment extends DialogFragment {

        public CBDialogFragment(int id)
        {
            super();
            Bundle args = new Bundle();
            args.putInt("id", id);
            this.setArguments(args);

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int id = getArguments().getInt("id");
            if(id == PROBLEM_DIALOG) {
               return new AlertDialog.Builder( getActivity() )
                        .setTitle(R.string.select_error_label)
                        .setMultiChoiceItems(R.array.error_report_items, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items

                                    selectedItems.add(which);
                                } else if (selectedItems.contains(which)) {
                                    // Else, if the item is already in the array, remove it
                                    selectedItems.remove(Integer.valueOf(which));
                                }
                                updateSelectedItems();

                            }
                        })
                        .setPositiveButton("OK", new DialogButtonClickHandler())
                        .create();
            } else if(id == LOCK_NUMBER_DIALOG) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.service_lock_number_text);
                builder.setItems(R.array.service_lock_numbers, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        selectedLockNumber = Integer.toString(which+1);
                        lockNumberButton.setText(getActivity().getString(R.string.service_lock_number_text) + "(" + selectedLockNumber + ")");
                    }
                });
                return builder.create();
            }
            return null;

        }
        @Override
        public void onResume() {
            super.onResume();
            int id = getArguments().getInt("id");
            if(id == PROBLEM_DIALOG) {
            Dialog dialog = getDialog();
            ListView lv =((AlertDialog)dialog).getListView();
            for (int i=0; i<checkedItems.length; i++){
                lv.setItemChecked(i, checkedItems[i]);
            }
            }
            // reset code goes here - use dialog as you would have in onPrepareDialog()
        }


    }

}
