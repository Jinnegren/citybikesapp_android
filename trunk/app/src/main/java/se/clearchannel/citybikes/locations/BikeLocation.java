package se.clearchannel.citybikes.locations;

import com.google.android.gms.maps.model.LatLng;

public class BikeLocation {

	private Long id;
	private String name;
	private LatLng location;
	private BikeLocationData data;
	private Float distanceToMyLocation;
	
	
	public BikeLocation(Long id, String name, LatLng location) {
		super();
		this.id = id;
		this.name = name;
		this.location = location;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LatLng getLocation() {
		return location;
	}
	public void setLocation(LatLng location) {
		this.location = location;
	}
	
	public BikeLocationData getLocationData() {
		return data;
	}
	public void setLocationData(BikeLocationData data) {
		this.data = data;
	}
	public Float getDistanceToMyLocation() {
		return distanceToMyLocation;
	}
	public void setDistanceToMyLocation(Float dist) {
		this.distanceToMyLocation = dist;
	}

    public String toString() {
        return name;
    }
	
	
}
