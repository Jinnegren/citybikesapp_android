package se.clearchannel.citybikes;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

/**
 * Created by perj on 2014-03-26.
 */
public class AboutFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = (View) inflater.inflate(R.layout.about, container, false);
        WebView myWebView = (WebView) mainView.findViewById(R.id.about_webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setDefaultTextEncodingName("UTF-8");

        String locale = getString(R.string.asset_locale_prefix);
        myWebView.loadUrl("file:///android_asset/" + locale + "/about.html");
        return mainView;

    }
}
