package se.clearchannel.citybikes;

import android.content.Context;

/**
 * Created by perj on 2014-03-26.
 */
public interface RefreshListener {
    public void onRefresh(MainActivity activity);
}